source ./env.sh
if [ -n "$env" ]; then
  docker compose -f docker-compose.yml up $@
else
  docker compose up $@
fi

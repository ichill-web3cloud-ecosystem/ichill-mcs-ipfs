import os
import uuid
import urllib.request
from flask import Flask, request
from werkzeug.utils import secure_filename
from dotenv import load_dotenv
from mcs import MetaSpaceAPI
from mcs.common.params import Params
from flask_cors import CORS

load_dotenv(".env")
UPLOAD_FOLDER = os.getenv('UPLOAD_FOLDER')
BUCKET_NAME = os.getenv('BUCKET_NAME')
CHAIN_NAME = os.getenv('CHAIN_NAME')
WALLET_ADDRESS = os.getenv('WALLET_ADDRESS')
WALLET_PRIVATE_KEY = os.getenv('WALLET_PRIVATE_KEY')
RPC_ENDPOINT = os.getenv('RPC_ENDPOINT')

ALLOWED_EXTENSIONS = {'mp4', 'avi', 'flv', 'mkv', 'webm', 'wmv'}

meta_space_api = MetaSpaceAPI(Params(CHAIN_NAME).MCS_API)


def allowed_file(filename):
  return ('.' in filename) and \
      (filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS)


app = Flask(__name__)
CORS(app)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route("/")
def hello_world():
  return "<p>Hello, World!</p>"


def RespData(status, message, data=""):
  return {
    "status": status,
    "message": message,
    "data": data
  }


@app.route('/upload', methods=['POST'])
def upload_file():
  if request.method == 'POST':
    if 'file' not in request.files:
      return RespData("failed", "No file part")

    file = request.files['file']
    if file.filename == '':
      return RespData("failed", "No selected file")

    if file and allowed_file(file.filename):
      filename = secure_filename(file.filename)

      filekey = (uuid.uuid4()).hex + '.' + \
          filename.rsplit('.', 1)[1].lower()

      file.save(os.path.join(app.config['UPLOAD_FOLDER'], filekey))

      meta_space_api.get_jwt_token(
        WALLET_ADDRESS, WALLET_PRIVATE_KEY, CHAIN_NAME)
      mcsupload = meta_space_api.upload_to_bucket(
        BUCKET_NAME, filekey, os.path.join(app.config['UPLOAD_FOLDER'], filekey))

      os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filekey))
      if (mcsupload['status'] == 'success'):
        return RespData("success", "Upload success ", filekey)
      return RespData("failed", "Upload failed")
    else:
      return RespData("failed", "Forbidden")


@app.route('/download', methods=['GET'])
def download_file():
  file_name = secure_filename(
    request.args.get('filename', default='', type=str))
  if not file_name:
    return RespData("failed", "file_name was empty")

  meta_space_api.get_jwt_token(WALLET_ADDRESS, WALLET_PRIVATE_KEY, CHAIN_NAME)
  bucket_info = meta_space_api.get_bucket_info(BUCKET_NAME)

  objects = bucket_info['data']['objects']
  for object in objects:
    if object['name'] == file_name:
      url = object['ipfs_url']
      break

  return urllib.request.urlopen(url)

@app.route('/file_url', methods=['GET'])
def get_file_url():
  file_name = secure_filename(
    request.args.get('filename', default='', type=str))
  if not file_name:
    return RespData("failed", "file_name was empty")

  meta_space_api.get_jwt_token(WALLET_ADDRESS, WALLET_PRIVATE_KEY, CHAIN_NAME)
  bucket_info = meta_space_api.get_bucket_info(BUCKET_NAME)

  objects = bucket_info['data']['objects']
  for object in objects:
    if object['name'] == file_name:
      url = object['ipfs_url']
      break

  return url
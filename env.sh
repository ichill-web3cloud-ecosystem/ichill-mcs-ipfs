export backend_port=8001
export mysql_port=8002
export web_port=8003
export meilisearch_port=8006
export ipfs_port=8008
export redis_backend_port=8004
export redis_web_port=8005

export ipfs_hostname=${env}.ipfs

export network=${env}_ichill

export volumes_dir=/data/volumes
export config_dir=/data/config
export backup_dir=/data/backup

export ipfs_env=${config_dir}/${env}/ipfs/ipfs.env
export ipfs_upload=${volumes_dir}/${env}/ipfs/upload


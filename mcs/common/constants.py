MCS_POLYGON_MAIN_API = "https://api.multichain.storage/api/"
MCS_POLYGON_MUMBAI_API = "https://calibration-mcs-api.filswan.com/api/"
MCS_BSC_API = 'https://calibration-mcs-bsc.filswan.com/api/'
METASPACE_API = "https://meta-api.multichain.storage/api/"
GET = "GET"
POST = "POST"
PUT = "PUT"
DELETE = "DELETE"
FIL_PRICE_API = "https://api.filswan.com/stats/storage"
# mcs api
MCS_PARAMS = "v1/common/system/params"
PRICE_RATE = "v1/billing/price/filecoin"
PAYMENT_INFO = "v1/billing/deal/lockpayment/info"
TASKS_DEALS = "v1/storage/tasks/deals"
MINT_INFO = "v1/storage/mint/info"
UPLOAD_FILE = "v1/storage/ipfs/upload"
DEAL_DETAIL = "v1/storage/deal/detail/"
USER_REGISTER = "v1/user/register"
USER_LOGIN = "v1/user/login_by_metamask_signature"
# meta space api
DIRECTORY = "v3/directory"
DELETE_OBJECT = "v3/object"
UPLOAD_SESSION = "v3/file/upload"
# contract
USDC_ABI = "ERC20.json"
SWAN_PAYMENT_ABI = "SwanPayment.json"
MINT_ABI = "SwanNFT.json"

CONTRACT_TIME_OUT = 300

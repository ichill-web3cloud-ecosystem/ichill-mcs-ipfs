FROM python:3.9.16-alpine
RUN pip install Flask
RUN pip install python-dotenv

# 1 install require package to run mcs local
RUN pip install web3
RUN pip install -U requests
RUN pip install requests-toolbelt
RUN pip install tqdm
RUN pip install -U flask-cors

# 2 install mcs sdk
# RUN pip install python-mcs-sdk

RUN apk --no-cache add curl
WORKDIR /usr/src/app
COPY . .
CMD ["python3", "-m", "flask", "--app", "main.py", "run", "--host=0.0.0.0"]